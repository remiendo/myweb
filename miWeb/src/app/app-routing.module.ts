	import { NgModule } from '@angular/core';
	import { Routes, RouterModule } from '@angular/router';
	import {HomeComponent} from './home/home.component';
	import {PortadaComponent} from './portada/portada.component';
	import {StatementComponent} from './statement/statement.component';
	import {FichadetalleComponent} from './fichadetalle/fichadetalle.component';
	import {ContactComponent} from './contact/contact.component';
	import {VideoComponent} from './video/video.component';

	const routes: Routes = [
	{path: 'home' , component: HomeComponent },
	{path: 'portada' , component: PortadaComponent},
	{path: 'fichadetalle/:id', component: FichadetalleComponent},
	{path: 'video' , component: VideoComponent},
	{path: 'statement', component: StatementComponent},
	{path: 'contact' , component: ContactComponent},
	{path: '', redirectTo: '/home', pathMatch: 'full'}
	];

	@NgModule({
		imports: [RouterModule.forRoot(routes)],
		exports: [RouterModule]
	})
	export class AppRoutingModule { }
