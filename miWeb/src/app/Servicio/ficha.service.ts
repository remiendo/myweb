import { FICHAS }  from  '../Compartidos/fichas';
import { Ficha } from '../Compartidos/fichatécnica';
import { Contacto } from '../Compartidos/contacto';
import { Observable, of, throwError} from 'rxjs';
import { delay, catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ConfigURL } from '../Compartidos/ConfigUrl';


@Injectable({
	providedIn: 'root'
})

//@Injectable()
export class FichaService {

 private configUrl = 'api/fichas/';
 private ConfigUrl = 'api/Contacto/';

	getFichas(): Observable<Ficha[]> {
                                 //(ConfigURL + 'fichas' )
		return this.http.get<Ficha[]>(this.configUrl ).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );;
		//of(FICHAS).pipe(delay(2000));
	}

	getFicha(id: any ): Observable<Ficha> {
                            //(ConfigURL + 'fichas/' + id)
		return this.http.get<Ficha>(this.configUrl + id).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
		//of(FICHAS.filter((ficha) => (ficha.id === id))[0]).pipe(delay(2000));
	}

	getFichaIds(): Observable<number[] | any> {
		return this.getFichas().pipe(map(fichas => fichas.map(ficha => ficha.id))).pipe(
      retry(2),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    );
		//of(FICHAS.map(ficha => ficha.id));
	}

	httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

	postContacto(contacto: Contacto): Observable<Contacto>{
		                              //(ConfigURL + 'Contacto', contacto, this.httpOptions )
		return this.http.post<Contacto>(this.configUrl + 'contacto', contacto, this.httpOptions ).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError(error);
      })
    )
	}

putFicha(ficha: Ficha): Observable<Ficha> {
                              //(ConfigURL + 'fichas/' + ficha.id, ficha, this.httpOptions )
    return this.http.put<Ficha>(this.configUrl  + ficha.id, ficha, this.httpOptions );
  }
	constructor(private http: HttpClient) { }




}
