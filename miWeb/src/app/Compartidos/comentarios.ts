export interface Comentario {
	estrellas: number;
	opinion: string;
	autor: string;
	fecha: any;
}