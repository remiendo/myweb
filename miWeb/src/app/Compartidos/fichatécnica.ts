import {Comentario} from './comentarios';

export interface Ficha {
	id: any;
	titulo: string;
	image: string;
	tecnica: string;
	ano: number;
	comentario: Comentario [];

}