import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
//import { FichasModule } from './fichadetalle/fichas.module';

import { FichaService} from './Servicio/ficha.service';
import { DataService } from './data.service';

import { ConfigURL } from './Compartidos/ConfigUrl';

import { AppComponent } from './app.component';
import { PortadaComponent } from './portada/portada.component';
import { FichadetalleComponent } from './fichadetalle/fichadetalle.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { StatementComponent } from './statement/statement.component';
import { ContactComponent } from './contact/contact.component';
import { VideoComponent } from './video/video.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    PortadaComponent,
    FichadetalleComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    StatementComponent,
    ContactComponent,
    VideoComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSliderModule,
    HttpClientModule,
    //FichasModule,
    HttpClientInMemoryWebApiModule.forRoot(DataService)
  ],
  entryComponents: [ LoginComponent ],
  providers: [FichaService, { provide: 'configUrl', useValue: ConfigURL} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
