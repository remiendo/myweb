import { Component, OnInit, Inject } from '@angular/core';
import { Ficha } from '../Compartidos/fichatécnica';
//import { Comentario } from '../Compartidos/comentarios';
//import {FICHAS} from '../Compartidos/fichas';
import {FichaService} from '../Servicio/ficha.service';



		
@Component({
	selector: 'app-portada',
	templateUrl: './portada.component.html',
	styleUrls: ['./portada.component.css']
})

export class PortadaComponent implements OnInit {

	fichas: Ficha [];
	//seleccionFicha: Ficha; 
	//data : Ficha[];
	constructor(private fichaServicio: FichaService, @Inject('configUrl') public configUrl) { }

	ngOnInit() {

		this.fichaServicio.getFichas().subscribe(fichas => this.fichas = fichas);
	}

	

			/*{

				getFichas(){
		this.fichaServicio.getFichas().subscribe((data : Ficha [] )=> this.fichas = { ...data 	});
	}
				id: (data as any).id,
			titulo: (data as any).titulo,
			image: (data as any).image,
			tecnica: (data as any).tecnica,
			ano: (data as any).ano,
			comentario: [ (data as any).comentario]
				estrellas: (data as any).estrellas,
				opinion: (data as any).opinion,
				autor: (data as any).autor,
				fecha: ((data as any) as any).fecha
			}*/
	
	
		

/*onSelect(ficha: Ficha) {
	this.seleccionFicha = ficha;
} */

}
