import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FichaService } from '../Servicio/ficha.service';
import { Ficha } from '../Compartidos/fichatécnica';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  Comentario  } from '../Compartidos/comentarios';

//import { PortadaComponent } from '../portada/portada.component';

@Component({
  selector: 'app-fichadetalle',
  templateUrl: './fichadetalle.component.html',
  styleUrls: ['./fichadetalle.component.css']
})
export class FichadetalleComponent implements OnInit {

 
  ficha: Ficha;

  fichaIds: string[];
  prev: string;
  next: string;
  comment: Comentario;
  comentarioform: FormGroup;
  fichacopy: Ficha;


  /*comentario = {

    comentario: '',
    id: null
  }*/

  edit = true;
  add = false;
  

  constructor( private fichaServicio: FichaService,
   private route: ActivatedRoute, private location: Location, private fb: FormBuilder, 
    @Inject('configUrl') public configUrl ) {
  
   this.createForm();
    }

createForm(): void {
  this.comentarioform = this.fb.group ({
  estrellas: ['', [Validators.required]],
  opinion: ['', [Validators.required, Validators.minLength(4)]],
  autor: ['', [Validators.required, Validators.minLength(3)]],
  fecha: ''
    
  });
  }

get estrellas() {
    return this.comentarioform.get('estrellas');
  }

 get opinion() {
    return this.comentarioform.get('opinion');
  }
get autor() {
    return this.comentarioform.get('autor');
  }

    ngOnInit() {

this.fichaServicio.getFichaIds().subscribe(fichaIds => this.fichaIds = fichaIds); 
this.route.params.pipe(switchMap( (params: Params) => this.fichaServicio.getFicha(params['id'])))
.subscribe(ficha => { this.ficha = ficha; this.fichacopy= ficha; this.setPrevNext(ficha.id)})
    //const id = this.route.snapshot.params['id'];
    //this.ficha = this.fichaServicio.getFicha(id);
  }


  onSubmit() {

   
    this.comment = this.comentarioform.value;
    this.comment.fecha = new Date().toISOString();
    console.log(this.comment);
    this.fichacopy.comentario.push(this.comment);
    this.fichaServicio.putFicha(this.fichacopy)
    .subscribe(ficha => {
      this.ficha = ficha; this.fichacopy = ficha; });
    /* this.fichaServicio.postFicha(this.comment)
    .subscribe(comment => {
      this.comment[""].push(this.comment) 
      });*/
      this.comentarioform.reset({
      
     estrellas: '',
     opinion: '', 
     autor: '', 
     fecha: ''
   });
     
   }


  setPrevNext(fichaId: string){
    const index = this.fichaIds.indexOf(fichaId);
    this.prev = this.fichaIds[(this.fichaIds.length + index - 1) % this.fichaIds.length];
    this.next = this.fichaIds[(this.fichaIds.length + index + 1) % this.fichaIds.length];
  }

   goBack(): void {
      this.location.back();
    }

}
