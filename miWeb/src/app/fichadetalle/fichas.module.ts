/*import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichadetalleComponent } from './fichadetalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FichadetalleComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [FichadetalleComponent]
})
export class FichasModule { }