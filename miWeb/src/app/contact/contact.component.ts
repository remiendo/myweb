import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FichaService } from '../Servicio/ficha.service';
import { Contacto } from '../Compartidos/contacto';



@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
contactform: FormGroup;
contacto: Contacto;
contactocopy: Contacto;

constructor(private fichaServicio: FichaService, private fb: FormBuilder, @Inject('configUrl') public configUrl) { 

  this.createForm();

  }

createForm(): void {
   this.contactform = this.fb.group ({
      Nombre: ['', [Validators.required, Validators.minLength(4) ]],
      Apellido: ['', [Validators.required, Validators.minLength(4)]],
      Email: ['', Validators.email],
      Comentario: ['', [Validators.required, Validators.minLength(3)]]
    });
}
  ngOnInit(): void {

    
   
  }


  get Nombre() {
    return this.contactform.get('Nombre');
  }
  get Apellido() {
    return this.contactform.get('Apellido');
  }

   get Email() {
    return this.contactform.get('Email');
  }

  get Comentario() {
    return this.contactform.get('Comentario');
  }

  onSubmit() {
  // TODO: Use EventEmitter with form value
  this.contacto = this.contactform.value;
  console.warn(this.contacto);
  //this.contactocopy = this.contacto;
  //console.log(this.contactform);
  this.fichaServicio.postContacto(this.contacto)
  .subscribe(contacto => this.contacto =contacto );
  this.contactform.reset({

    Nombre: '',
    Apellido: '',
    Email: '',
    Comentario:''
  });
}

  


 
    
  



}
